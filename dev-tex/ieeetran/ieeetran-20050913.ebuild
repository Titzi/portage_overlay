# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit latex-package

DESCRIPTION="The IEEE Transactions LaTeX and BibTeX classes and styles"
HOMEPAGE="http://www.ieee.org/portal"
LATEX_SRC="IEEEtran.tar.gz"
BIBTEX_SRC="IEEEtranBST.tar.gz"
SRC_PATH="http://www.ieee.org/portal/cms_docs/pubs/transactions"
SRC_URI="${SRC_PATH}/${LATEX_SRC}
${SRC_PATH}/${BIBTEX_SRC}"

LICENSE="Artistic"
KEYWORDS="~x86 ~ppc-macos"

src_unpack() {
	unpack ${LATEX_SRC} 
	unpack ${BIBTEX_SRC}
}

src_install() {
	cd ${WORKDIR}
	
	# Installing LaTeX
	einfo "Installing LaTeX class"
	insinto ${TEXMF}/tex/latex/${PN}
	doins "IEEEtran.cls" || die "doins IEEEtran.cls failed"

	# Documentation. We'll put the example files there too
	einfo "Installing documentation"
	for i in "IEEEtran_HOWTO.pdf" "bare_conf.tex" "bare_jrnl.tex" "IEEEtran_bst_HOWTO.pdf" "IEEEexample.bib"
	do
		insinto /usr/share/doc/${P}
		doins $i || die "doins $i failed"
	done
	
	# Bibliography styles
	einfo "Installing bibliography styles"
	for i in "IEEEtran.bst" "IEEEtranS.bst"
	do
		insinto ${TEXMF}/bibtex/bst/${PN}
		doins $i || die "doins $i failed"
	done
	
	# Bibliography files
	einfo "Installing BibTeX files"
	for i in "IEEEabrv.bib" "IEEEbcpat.bib" "IEEEfull.bib"
	do
		insinto ${TEXMF}/bibtex/bib/${PN}
		doins $i || die "doins $i failed"
	done
}
