# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

#VIM_PLUGIN_VIM_VERSION="7.0"
inherit vim-plugin

P="calendar.vim"

DESCRIPTION="vim plugin: adding a calendar to vim"
HOMEPAGE="https://github.com/itchyny/calendar.vim"
#SRC_URI="https://github.com/itchyny/calendar.vim/archive/master.tar.gz -> ${P}.tar.gz"
EGIT_REPO_URI="git@github.com:itchyny/calendar.vim.git"
LICENSE="MIT"
KEYWORDS="~amd64"
IUSE=""

VIM_PLUGIN_HELPFILES=""
VIM_PLUGIN_HELPTEXT=""
VIM_PLUGIN_HELPURI=""
VIM_PLUGIN_MESSAGES=""
